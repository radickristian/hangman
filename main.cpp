#include <iostream>
#include "Hangman.h"

int main() {

    std::cout << "\n Zadaj protivniku JEDNU riječ, upri enter i gledaj ga kako faliva:" << "\nHangman s više riječi dostupan samo za premium korisnike \n";
    std::string word;

    std::cin >> word;

    std::cout << std::string( 100, '\n' );

    Hangman hangman(word);
    hangman.BeginGame();

    return 0;
}