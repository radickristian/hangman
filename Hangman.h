//
// Created by kristian on 2019-06-04.
//

#ifndef HANGMAN_HANGMAN_H
#define HANGMAN_HANGMAN_H

#include <string>
#include "Game.h"

class Hangman : public Game {
private:
    std::string m_gameProgress = "";
    int m_matchedLettersCount = 0;
    bool m_victoryRoyal = false;
    bool m_isMatch = false;


public:

    Hangman(std::string wordToGuess);

    Hangman BeginGame();

    Hangman SetHangman(char letter = 0);

    std::string GenerateGameProgress(char letter = 0);

    Hangman IncrementMatchedLettersCount();

    bool IsVictoryRoyal();

    std::string getGameProgress() { return m_gameProgress; }

    int getMatchedLettersCount() { return m_matchedLettersCount; }

};

#endif //HANGMAN_HANGMAN_H
