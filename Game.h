//
// Created by kristian on 2019-06-04.
//

#ifndef HANGMAN_GAME_H
#define HANGMAN_GAME_H

#include <string>

class Game {
protected:

    std::string m_wordToGuess = "";
    int m_numberOfTries;

public:
    Game(std::string wordToGuess);

    Game SetGame(std::string wordToGuess);

    Game DecrementNumberOfTries();

    std::string TrimWord(std::string word);

    std::string getWordToGuess() { return m_wordToGuess; }

    int getNumberOfTries() { return m_numberOfTries; }
};


#endif //HANGMAN_GAME_H
