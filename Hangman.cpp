//
// Created by kristian on 2019-06-04.
//

#include "Hangman.h"
#include <iostream>

Hangman::Hangman(std::string wordToGuess) : Game(wordToGuess) {

    Game game(wordToGuess);
    SetHangman();

}

Hangman Hangman::SetHangman(char letter) {

    m_gameProgress = GenerateGameProgress(letter);

    return *this;
}

std::string Hangman::GenerateGameProgress(char letter) {

    std::string hiddenWord;

    if (m_gameProgress == "") {
        std::string hidden_word(m_wordToGuess.length(), '-');
        hiddenWord = hidden_word;
    } else {
        hiddenWord = m_gameProgress;
    }

    if (letter == 0) {
        return hiddenWord;
    }

    int i;
    int len = m_wordToGuess.length();
    int matches = 0;
    for (i = 0; i < len; i++) {
        if (letter == m_wordToGuess[i]) {
            m_isMatch = true;
            hiddenWord[i] = letter;
            IncrementMatchedLettersCount();
            matches++;
        }
    }

    if (matches == 0) {
        m_isMatch = false;
        DecrementNumberOfTries();
    }

    m_gameProgress = hiddenWord;

    return hiddenWord;
}

Hangman Hangman::IncrementMatchedLettersCount() {

    m_matchedLettersCount = m_matchedLettersCount + 1;

    return *this;
}


bool Hangman::IsVictoryRoyal() {

    if (m_wordToGuess.length() == m_matchedLettersCount)
        m_victoryRoyal = true;

    return m_victoryRoyal;
}


Hangman Hangman::BeginGame() {

    char letter;

    int numberOfTries = m_numberOfTries;

    std::cout << "\nRiječ: \n";
    std::cout << m_gameProgress;

    while (numberOfTries != 0) {
        std::cout << "\nPogodi slovo: \n";
        std::cin >> letter;

        m_isMatch = false;
        GenerateGameProgress(letter);

        std::cout << m_gameProgress;

        if (!m_isMatch) {
            std::cout << "\nNetočno slovo.\n Broj preostalih pokušaja: " << m_numberOfTries ;
        }

        if (IsVictoryRoyal()) {
            std::cout << "\nToooo, pa ti si najbolji ikad <3!\n" << std::endl;
            break;
        }

        numberOfTries = m_numberOfTries;
    }

    if (numberOfTries == 0) {
        std::cout << "\nJednostavnia riječ nije mogla bit, katastrofalan si.\n";
        std::cout << "\nRiječ je bila: " << m_wordToGuess << std::endl;
    }

    return *this;
}