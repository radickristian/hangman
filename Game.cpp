//
// Created by kristian on 2019-06-04.
//

#include "Game.h"


Game::Game(std::string wordToGuess) {

    SetGame(wordToGuess);
}

Game Game::SetGame(std::string wordToGuess) {

    m_wordToGuess = TrimWord(wordToGuess);
    m_numberOfTries = wordToGuess.length();

    return *this;
}

Game Game::DecrementNumberOfTries() {
    m_numberOfTries--;

    return *this;
}

std::string Game::TrimWord(std::string word) {

    size_t first = word.find_first_not_of(' ');
    if (std::string::npos == first) {
        return word;
    }
    size_t last = word.find_last_not_of(' ');
    //ovo san kopira sa stack overflowa (funkcija za trimanje stringa - razmaka)

    return word.substr(first, (last - first + 1));
}



